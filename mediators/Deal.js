const Base = require('./Base');
const queryString = require('querystring');

class Deal extends Base {
    constructor(client) {
        super(client);
    }
    getInfo(id) {
        const url = '/api/v3/deal/' + id;
        return this.get(url);
    }
    getList(list) {
        const url = '/api/v3/deal' + queryString.stringify(list);
        return this.get(url);
    }
    create(deal) {
        const url = '/api/v3/deal';
        return this.post(url, deal);
    }
    update(deal) {
        const url = '/api/v3/deal' + deal.id;
        return this.post(url, deal);
    }
    remove(id) {
        const url = '/api/v3/deal/' + id;
        return this.delete(url);
    }
    getLinkedTasks(id, list) {
        const url = '/api/v3/deal/' + id + '/linkedTasks' + queryString.stringify(list);
        return this.get(url);
    }
    getLinkedDeals(id, list) {
        const url = '/api/v3/deal/' + id + '/linkedDeals' + queryString.stringify(list);
        return this.get(url);
    }
    getComments(id, list) {
        const url = '/api/v3/deal/' + id + '/comments' + queryString.stringify(list);
        return this.get(url);
    }
    createTodo(id, todo) {
        const url = '/api/v3/deal/' + id + '/todos';
        return this.post(url, todo);
    }
}

module.exports = Deal;