class Base {
    constructor(client) {
        this.client = client;
    }
    get(url) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await this.client.get(url);
                if (response.data.meta && response.data.meta.status === 200) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                    // TODO: add logging...
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    post(url, data) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await this.client.post(url, data);
                if (response.data.meta && response.data.meta.status === 200) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                    // TODO: add logging...
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    delete(url) {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await this.client.delete(url);
                if (response.data.meta && response.data.meta.status === 200) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                    // TODO: add logging...
                }
            } catch (e) {
                reject(e);
            }
        });
    }
}

module.exports = Base;