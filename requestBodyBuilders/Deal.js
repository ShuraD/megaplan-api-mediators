class Deal {
    constructor() {
        this.programId = null;
        this.id = null;
        this.name = null;
        this.extraFields = [];
    }
    setId(id) {
        this.id = id;
        return this;
    }
    getId() {
        return this.id;
    }
    setName(name) {
        this.name = name;
    }
    addExtraField(field, value) {
        const fieldObject = {};
        fieldObject[field] = value;
        this.extraFields.push(fieldObject);
    }
}

module.exports = Deal;